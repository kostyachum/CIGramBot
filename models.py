from peewee import Model, CharField, BooleanField
from playhouse.sqlite_ext import SqliteExtDatabase
from settings import config

db = SqliteExtDatabase(config.DATABASE_NAME)


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    username = CharField()
    can_deploy = BooleanField(default=False)
    telegram_id = CharField(unique=True)
