import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

DATABASE_NAME = 'cigram.db'
BOT_TOKEN = ''

DEPLOY_SCRIPT = './tube-deploy.sh'
DEPLOY_DIRECTORY = '/home'

try:
    from local import *
except ImportError:
    pass
