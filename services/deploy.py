from subprocess import call


class DeployService(object):
    """
    Helps to run script
    """
    def __init__(self, user, deploy_script, directory):
        self.user = user
        self.deploy_script = deploy_script
        self.directory = directory

    def deploy(self):
        """
        If user have permission - runs script,
        returns success/fail message
        """
        if not self.user.can_deploy:
            return "You are not allowed to deploy, sorry"

        try:
            call(self.deploy_script, cwd=self.directory)
            return 'Done, check it out now.'
        except OSError as e:
            return 'Deploying failed: {}'.format(e.message)
