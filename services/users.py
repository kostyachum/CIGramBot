from models import User


class UserService(object):
    """
    Helps to work with users,
    will create new one if doesn't exists,
    will grand deploy permission if is the first user
    """
    def __init__(self, message):
        user, created = User.get_or_create(
            telegram_id=message.chat_id, defaults=dict(username=message.from_user.username)
        )
        self.user = user
        self.created = created

        if self.created and User.select().count() == 1:
            self.user.can_deploy = True
            self.user.save()

    def get_user(self):
        return self.user

    def get_start_message(self):
        """
        Returns greeting message
        """
        if not self.created:
            return "You already registered..."

        hello_line = "Hello, <strong>{username}</strong>!\nYou've been registered.".format(username=self.user.username)
        line = self._append_deploy_permission(hello_line)
        return line

    def _append_deploy_permission(self, hello_line):
        if self.user.can_deploy:
            return "{}\nYou can deploy now.".format(hello_line)
        return "{}\nBut, you can not deploy and I'm useless for you at the moment.".format(hello_line)
