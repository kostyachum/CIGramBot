Cigram
=======

Telegram bot that runs script on server it's running

Configuration
-------------

- settings.config.DATABASE_NAME -- name for the database file
- settings.config.BOT_TOKEN -- your bot token
- settings.config.DEPLOY_SCRIPT -- command to run your script, for example './deploy.sh'
- settings.config.DEPLOY_DIRECTORY -- directory where script is located


Usage
-----

1. Run your bot on the same machine where your server (or at least script) is located;
2. Send `/start` to register;
3. Send `/deploy` to run script.

First user, who sent `start` or `deploy` will be granted with deploy permissions.
All the other users will be created, but no use so far.
