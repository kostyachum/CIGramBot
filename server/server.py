from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from settings import config
import handlers


class BotServer(object):
    """
    Telegram server with handlers mapped to commands
    """
    HANDLER_MAPPING = {
        "start": handlers.start_handler,
        "deploy": handlers.deploy_handler,
        "help": handlers.help_handler,
    }

    def run(self):
        """
        Initialize bot and attach handlers to the bot,
        polling forever
        """
        updater = Updater(config.BOT_TOKEN)
        dp = updater.dispatcher

        self._add_handlers(dp)
        dp.add_handler(MessageHandler([Filters.text], handlers.echo_handler))
        dp.add_error_handler(handlers.error_handler)
        updater.start_polling()
        updater.idle()

    def _add_handlers(self, dp):
        for handler in self.HANDLER_MAPPING:
            dp.add_handler(CommandHandler(handler, self.HANDLER_MAPPING.get(handler)))
