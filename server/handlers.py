import logging
from telegram import ParseMode

from services.deploy import DeployService
from services.users import UserService
from settings import config
logger = logging.getLogger(__name__)


def start_handler(bot, update):
    """
    Will register new user
    or no
    """
    bot.sendMessage(update.message.chat_id, text='loading profile...')
    user_service = UserService(update.message)
    bot.sendMessage(update.message.chat_id, text=user_service.get_start_message(), parse_mode=ParseMode.HTML)


def help_handler(bot, update):
    """
    Prints help
    """
    bot.sendMessage(update.message.chat_id, text='Send "start" to register\nSend "deploy" do run deployment')


def echo_handler(bot, update):
    """
    Says 'Sorry?' on unknown command
    """
    bot.sendMessage(update.message.chat_id, text="Sorry?")


def error_handler(bot, update, error):
    """
    Logs telegram errors
    """
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def deploy_handler(bot, update):
    """
    Handles deploy command,
    will run deploy process if user have permissions
    """
    user_service = UserService(update.message)

    if user_service.created:
        bot.sendMessage(update.message.chat_id, user_service.get_start_message(), parse_mode=ParseMode.HTML)

    bot.sendMessage(update.message.chat_id, text='Runing {}'.format(config.DEPLOY_SCRIPT), parse_mode=ParseMode.HTML)

    deploy_message = DeployService(
        user=user_service.get_user(),
        deploy_script=config.DEPLOY_SCRIPT,
        directory=config.DEPLOY_DIRECTORY
    ).deploy()
    bot.sendMessage(update.message.chat_id, text=deploy_message, parse_mode=ParseMode.HTML)
