from server.server import BotServer


def check_database():
    """
    Create tables if doesn't exists
    """
    from models import User
    try:
        User.select().count()
    except Exception:
        from models import db
        db.connect()
        db.create_tables([User])

if __name__ == '__main__':
    check_database()
    BotServer().run()
